-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Sep 12, 2016 at 05:43 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `copymachine`
--
CREATE DATABASE IF NOT EXISTS `copymachine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `copymachine`;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

DROP TABLE IF EXISTS `checklist`;
CREATE TABLE IF NOT EXISTS `checklist` (
  `checklist_number` int(11) NOT NULL,
  `checklist_detail` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` varchar(200) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `contact_name` varchar(30) NOT NULL,
  `contact_tel` varchar(20) NOT NULL,
  `contact_tel_mobile` varchar(20) DEFAULT NULL,
  `own_machine_model` varchar(10) NOT NULL,
  `own_machine_number` varchar(20) DEFAULT NULL,
  `remark` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
CREATE TABLE IF NOT EXISTS `inventory` (
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL COMMENT 'Number of parts left in inventory',
  `price` int(11) NOT NULL COMMENT 'Price of spar part'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_checklist`
--

DROP TABLE IF EXISTS `job_checklist`;
CREATE TABLE IF NOT EXISTS `job_checklist` (
  `job_number` varchar(20) NOT NULL,
  `checklist_number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_document`
--

DROP TABLE IF EXISTS `job_document`;
CREATE TABLE IF NOT EXISTS `job_document` (
  `job_number` varchar(20) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `issue_start_time` varchar(10) DEFAULT NULL,
  `issue_end_time` varchar(10) DEFAULT NULL,
  `technician_id` varchar(10) NOT NULL,
  `request_date` datetime NOT NULL,
  `symptom` varchar(500) DEFAULT NULL,
  `root_cause` varchar(500) DEFAULT NULL,
  `solution` varchar(500) DEFAULT NULL,
  `otherMaintenance` varchar(500) DEFAULT NULL,
  `fixed` smallint(6) DEFAULT NULL,
  `nextAction` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_meter`
--

DROP TABLE IF EXISTS `job_meter`;
CREATE TABLE IF NOT EXISTS `job_meter` (
  `job_number` varchar(20) NOT NULL,
  `start_meter` int(11) DEFAULT NULL,
  `end_meter` int(11) DEFAULT NULL,
  `black_a4` int(11) DEFAULT NULL,
  `color_a4` int(11) DEFAULT NULL,
  `black_a3` int(11) DEFAULT NULL,
  `color_a3` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_spare_part`
--

DROP TABLE IF EXISTS `job_spare_part`;
CREATE TABLE IF NOT EXISTS `job_spare_part` (
  `job_number` varchar(20) NOT NULL,
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `requisition_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `item_line_id` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

DROP TABLE IF EXISTS `requisition`;
CREATE TABLE IF NOT EXISTS `requisition` (
  `requisition_id` varchar(10) NOT NULL,
  `requisition_date` date NOT NULL,
  `approver_userid` varchar(6) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `receiver_userid` varchar(10) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `giver_userid` varchar(10) DEFAULT NULL,
  `give_date` date DEFAULT NULL,
  `receiver_onbehalf` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Requisition header';

-- --------------------------------------------------------

--
-- Table structure for table `requisition_item`
--

DROP TABLE IF EXISTS `requisition_item`;
CREATE TABLE IF NOT EXISTS `requisition_item` (
  `requisition_id` varchar(10) NOT NULL,
  `item_line_id` smallint(6) NOT NULL,
  `part_number` int(10) NOT NULL,
  `job_number` varchar(10) DEFAULT NULL,
  `customer_id` varchar(10) DEFAULT NULL,
  `borrow_amount` smallint(6) NOT NULL,
  `return_amount` smallint(6) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL,
  `Remark` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Triggers `requisition_item`
--
DROP TRIGGER IF EXISTS `requisition_add`;
DELIMITER $$
CREATE TRIGGER `requisition_add` AFTER INSERT ON `requisition_item`
 FOR EACH ROW UPDATE inventory
	SET quantity = quantity - NEW.borrow_amount
	WHERE part_number = NEW.`part_number`
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `requisition_delete`;
DELIMITER $$
CREATE TRIGGER `requisition_delete` AFTER DELETE ON `requisition_item`
 FOR EACH ROW UPDATE inventory
	SET quantity = quantity + OLD.borrow_amount
	WHERE part_number = OLD.`part_number`
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `spare_part`
--

DROP TABLE IF EXISTS `spare_part`;
CREATE TABLE IF NOT EXISTS `spare_part` (
  `part_number` int(10) NOT NULL COMMENT 'Spare part id',
  `part_name` varchar(100) NOT NULL,
  `part_description` varchar(100) DEFAULT NULL,
  `part_type` varchar(50) NOT NULL,
  `used_in_model` varchar(100) NOT NULL,
  `deter_rate` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `technician`
--

DROP TABLE IF EXISTS `technician`;
CREATE TABLE IF NOT EXISTS `technician` (
  `user_id` varchar(6) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nickname` varchar(30) NOT NULL,
  `id` int(13) DEFAULT NULL COMMENT 'Identification Number on Card',
  `tel1` varchar(13) DEFAULT NULL,
  `tel2` varchar(13) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`part_number`);

--
-- Indexes for table `job_checklist`
--
ALTER TABLE `job_checklist`
  ADD PRIMARY KEY (`job_number`,`checklist_number`);

--
-- Indexes for table `job_document`
--
ALTER TABLE `job_document`
  ADD PRIMARY KEY (`job_number`);

--
-- Indexes for table `job_spare_part`
--
ALTER TABLE `job_spare_part`
  ADD PRIMARY KEY (`job_number`,`part_number`);

--
-- Indexes for table `requisition`
--
ALTER TABLE `requisition`
  ADD PRIMARY KEY (`requisition_id`);

--
-- Indexes for table `requisition_item`
--
ALTER TABLE `requisition_item`
  ADD UNIQUE KEY `Main` (`requisition_id`,`item_line_id`);

--
-- Indexes for table `spare_part`
--
ALTER TABLE `spare_part`
  ADD PRIMARY KEY (`part_number`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
