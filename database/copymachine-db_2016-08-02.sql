-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 02, 2016 at 04:07 PM
-- Server version: 5.5.35
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `copymachine`
--
CREATE DATABASE IF NOT EXISTS `copymachine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `copymachine`;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

DROP TABLE IF EXISTS `checklist`;
CREATE TABLE `checklist` (
  `checklist_number` int(11) NOT NULL,
  `checklist_detail` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `checklist`
--

TRUNCATE TABLE `checklist`;
--
-- Dumping data for table `checklist`
--

INSERT INTO `checklist` (`checklist_number`, `checklist_detail`) VALUES
(1, 'เช็ดทำความสะอาดดรัม'),
(2, 'ทำความสะอาดชุด Feed กระดาษ'),
(3, 'ทำความสะอาดชุดโรเลอร์ชาร์จ'),
(4, 'ทำความสะอาดชุดเบลล์'),
(5, 'ทำความสะอาดชุด ADF'),
(6, 'ทำความสะอาดชุดกระจกสะท้อนภาพ'),
(7, 'ทำความสะอาดชุดทางเดินกระดาษ'),
(8, 'เปลียนโรลเลอร์ฮีทซิ้ง'),
(9, 'เติมหมึก'),
(10, 'เทหมึกเสียทิ้ง');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` varchar(200) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `contact_name` varchar(30) NOT NULL,
  `contact_tel` varchar(20) NOT NULL,
  `contact_tel_mobile` varchar(20) DEFAULT NULL,
  `own_machine_model` varchar(10) NOT NULL,
  `own_machine_number` varchar(20) DEFAULT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `customer`
--

TRUNCATE TABLE `customer`;
--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `customer_address`, `department`, `contact_name`, `contact_tel`, `contact_tel_mobile`, `own_machine_model`, `own_machine_number`, `remark`) VALUES
('A001-01', 'เอไอเอ (ชัวคราว)', '181 อาคารเอ.ไอ.เอ.พาวเวอร์ ถ.สุรวงศ์\r\n', NULL, 'คุณเอ๋', '0-2638-7651', NULL, 'IR3570', 'KFW12067', ''),
('A002-01', 'อนามัยภัณฑ์', 'สำโรง ชั้น 2 สุขุมวิท 76', NULL, 'คุณพรพนา', '0-2393-1608 ', NULL, 'IR3045', 'MUZ00257', '');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL COMMENT 'Number of parts left in inventory',
  `price` int(11) NOT NULL COMMENT 'Price of spar part',
  PRIMARY KEY (`part_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `inventory`
--

TRUNCATE TABLE `inventory`;
--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`part_number`, `quantity`, `price`) VALUES
(1, 20, 2500),
(2, 500, 350);

-- --------------------------------------------------------

--
-- Table structure for table `job_checklist`
--

DROP TABLE IF EXISTS `job_checklist`;
CREATE TABLE `job_checklist` (
  `job_number` int(11) NOT NULL,
  `checklist_number` int(11) NOT NULL,
  PRIMARY KEY (`job_number`,`checklist_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job_checklist`
--

TRUNCATE TABLE `job_checklist`;
--
-- Dumping data for table `job_checklist`
--

INSERT INTO `job_checklist` (`job_number`, `checklist_number`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `job_spare_part`
--

DROP TABLE IF EXISTS `job_spare_part`;
CREATE TABLE `job_spare_part` (
  `job_number` int(11) NOT NULL,
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`job_number`,`part_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job_spare_part`
--

TRUNCATE TABLE `job_spare_part`;
--
-- Dumping data for table `job_spare_part`
--

INSERT INTO `job_spare_part` (`job_number`, `part_number`, `quantity`) VALUES
(1, 1, 3),
(1, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

DROP TABLE IF EXISTS `requisition`;
CREATE TABLE `requisition` (
  `requisition_id` int(8) NOT NULL,
  `requisition_date` date NOT NULL,
  `approver_userid` varchar(6) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `receiver_userid` int(6) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `giver_userid` int(6) DEFAULT NULL,
  `give_date` date DEFAULT NULL,
  PRIMARY KEY (`requisition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Requisition header';

--
-- Truncate table before insert `requisition`
--

TRUNCATE TABLE `requisition`;
-- --------------------------------------------------------

--
-- Table structure for table `requisition_item`
--

DROP TABLE IF EXISTS `requisition_item`;
CREATE TABLE `requisition_item` (
  `requisition_id` int(8) NOT NULL,
  `item_line_id` smallint(6) NOT NULL,
  `item` varchar(50) NOT NULL,
  `model` varchar(10) NOT NULL,
  `job_number` varchar(10) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `borrow_amount` smallint(6) NOT NULL,
  `return_amount` smallint(6) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `Remark` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `requisition_item`
--

TRUNCATE TABLE `requisition_item`;
-- --------------------------------------------------------

--
-- Table structure for table `spare_part`
--

DROP TABLE IF EXISTS `spare_part`;
CREATE TABLE `spare_part` (
  `part_number` int(10) NOT NULL COMMENT 'Spare part id',
  `part_name` varchar(100) NOT NULL,
  `part_description` varchar(100) DEFAULT NULL,
  `part_type` varchar(50) NOT NULL,
  `used_in_model` varchar(10) NOT NULL,
  `deter_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`part_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `spare_part`
--

TRUNCATE TABLE `spare_part`;
--
-- Dumping data for table `spare_part`
--

INSERT INTO `spare_part` (`part_number`, `part_name`, `part_description`, `part_type`, `used_in_model`, `deter_rate`) VALUES
(1, 'BabyGanics Alcohol Free Foaming Hand Sanitizer', 'color ink', 'Ink', '877251784-', 500),
(2, 'Drum', NULL, 'Drum', 'SCX 8128', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `technician`
--

DROP TABLE IF EXISTS `technician`;
CREATE TABLE `technician` (
  `user_id` varchar(6) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nickname` varchar(30) NOT NULL,
  `id` int(13) DEFAULT NULL COMMENT 'Identification Number on Card',
  `tel1` varchar(13) DEFAULT NULL,
  `tel2` varchar(13) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `technician`
--

TRUNCATE TABLE `technician`;
--
-- Dumping data for table `technician`
--

INSERT INTO `technician` (`user_id`, `first_name`, `last_name`, `nickname`, `id`, `tel1`, `tel2`) VALUES
('jeng', NULL, NULL, 'เจ๋ง', NULL, '0934844940', NULL),
('ake', NULL, NULL, 'เอก', NULL, '0815495132', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_document`
--

DROP TABLE IF EXISTS `job_document`;
CREATE TABLE `job_document` (
  `job_number` int(11) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `issue_start_time` datetime DEFAULT NULL,
  `issue_end_time` datetime DEFAULT NULL,
  `technician_id` varchar(10) NOT NULL,
  PRIMARY KEY (`job_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job_document`
--

TRUNCATE TABLE `job_document`;
--
-- Dumping data for table `job_document`
--

INSERT INTO `job_document` (`job_number`, `customer_id`, `issue_start_time`, `issue_end_time`, `technician_id`) VALUES
(1, 'A001-01', NULL, NULL, 'jeng');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
