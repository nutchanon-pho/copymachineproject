app.controller('WithdrawController', function($scope, $http, $q) {
    $scope.month = month;
    $scope.monthShort = monthShort;
    $scope.weekdaysFull = weekdaysFull;
    $scope.weekdaysLetter = weekdaysLetter;

    $scope.inputItem = {};

    var promises = [
        $http({
            method: 'GET',
            url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + SPARE_PART_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + GET_JOB_REQUEST_LIST_ENDPOINT
        })
    ];

    $q.all(promises).spread(function(technicianListResponse, sparePartListResponse, customerListResponse, jobListResponse) {
        $scope.technicianList = technicianListResponse.data;
        console.log('$scope.technicianList', $scope.technicianList);
        $scope.sparePartInStockList = sparePartListResponse.data;
        $scope.sparePartMap = _.arrayToMap($scope.sparePartInStockList, 'id');
        console.log('$scope.sparePartMap', $scope.sparePartMap);
        console.log('$scope.sparePartList', $scope.sparePartInStockList);
        $scope.customerList = customerListResponse.data;
        $scope.customerMap = _.arrayToMap($scope.customerList, 'customer_id');
        console.log('$scope.customerList', $scope.customerList);
        $scope.jobList = jobListResponse.data;

        var allMachineModelNotUnique = _($scope.sparePartInStockList).chain().flatten().pluck('used_in_model').unique().value();
        var tempMachineModelList = [];
        $scope.machineModelList = [];
        _.each(allMachineModelNotUnique, function(val) {
            if (val) {
                var splittedArray = val.split(',');
                if(splittedArray){
                    _.each(splittedArray, function(modelName) {
                        modelName = modelName.toUpperCase();
                        var obj = {name : modelName};
                        if (!_.contains(tempMachineModelList, modelName)) {
                            tempMachineModelList.push(modelName);
                            $scope.machineModelList.push(obj);
                        }
                    });
                }
            }
        });
        console.log('$scope.machineModelList', $scope.machineModelList);
    });

    $scope.requestObj = {
        requisition_date: new Date(),
        number: '',
        bookNumber: '',
        requisition_item: []
    };

    $scope.inputItem = {};

    $scope.addItem = function() {
        var newItem = {
            model: '',
            itemName: '',
            job_number: '',
            customer: '',
            quantity: '',
            price: '',
            remark: ''
        };
        $scope.requestObj.lineItems.push(newItem);
    };

    $scope.selectJob = function(job) {
        $scope.inputItem.job_number = job.job_number;
        var customer = $scope.customerMap[job.customer_id];
        $scope.selectCustomer(customer);

        $scope.inputItem.hasJob = true;
    };

    $scope.selectCustomer = function(customer) {
        $scope.inputItem.customer = customer.customer_id + ' ' + customer.customer_name;
        $scope.inputItem.machineModel = customer.own_machine_model;
        $scope.inputItem.serialNumber = customer.own_machine_number;
        $scope.inputItem.hasCustomer = true;
        $scope.inputItem.itemName = '';
    }

    $scope.selectItem = function(item) {
		console.log('$scope.inputItem',$scope.inputItem);
        $scope.inputItem.itemName = item.part_name;
        $scope.inputItem.pricePerItem = item.price;
        $scope.inputItem.quanityInStock = item.quantity;
        $scope.inputItem.part_number = item.id;
    }

    $scope.addInputItem = function() {
        // var hasjob_number = $scope.inputItem.job_number && $scope.inputItem.job_number.length > 0;
        // var isValidSerialNumber = $scope.inputItem.serialNumber && $scope.inputItem.serialNumber.length > 0;
		console.log('$scope.inputItem',$scope.inputItem);
        var isValidItem = $scope.inputItem.itemName && $scope.inputItem.itemName.length > 0;
        var isValidQuantity = $scope.inputItem.quantity && ($scope.inputItem.quantity <= $scope.inputItem.quanityInStock);
        if (isValidItem && isValidQuantity) {
            useSparePart($scope.inputItem.part_number, $scope.inputItem.quantity);
            $scope.requestObj.requisition_item.push($scope.inputItem);
            $scope.inputItem.item_line_id = $scope.requestObj.requisition_item.length;
            $scope.inputItem = {};
            $scope.isShowInputItemCard = false;
        }
    }

    $scope.deleteItem = function(index) {
        var inputItem = $scope.requestObj.requisition_item[index];
        console.log('inputItem', inputItem);
        unUseSparePart(inputItem.part_number, inputItem.quantity);
        $scope.requestObj.requisition_item.splice(index, 1);
    }

    $scope.submit = function() {
        angular.forEach($scope.withdrawForm.$error.required, function(field) {
            field.$setDirty();
        });

        var isValidForm = $scope.withdrawForm.$valid;
		if(!isValidForm){
			alert('ใส่ข้อมูลไม่ครบ');
		}
		console.log('$scope.withdrawForm', $scope.withdrawForm);
        console.log('$scope.requestObj', $scope.requestObj);
        if (isValidForm) {
            $scope.requestObj.requisition_id = $scope.requestObj.bookNumber + '-' + $scope.requestObj.requisitionNumber;
            $http({
                method: 'POST',
                data: $scope.requestObj,
                url: SERVER_ENDPOINT + CREATE_REQUISITION_ENDPOINT
            }).then(function successCallback(response) {
                console.log(response);
                alert('สำเร็จ');
                location.reload();
            }, function errorCallback(response) {
                alert('Error');
                console.log(response);
            });
        }
    }

    function useSparePart(part_number, usedQuantity) {
        var usedSparePart = $scope.sparePartMap[part_number];
        if (usedSparePart) {
            if (usedSparePart.usedQuantity != null) {
                usedSparePart.usedQuantity += usedQuantity;
            } else {
                usedSparePart.usedQuantity = usedQuantity;
            }
        }
    }

    function unUseSparePart(part_number, usedQuantity) {
        var usedSparePart = $scope.sparePartMap[part_number];
        if (usedSparePart) {
            usedSparePart.usedQuantity -= usedQuantity;
        }
    }

    $scope.resetInputItem = function(){
        $scope.inputItem = {};
        $scope.isShowInputItemCard = false;  
    };
});
