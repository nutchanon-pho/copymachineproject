app.filter('customerFilterOnJobRequest', function() {
    return function(jobList, keyword) {
        if (!keyword) {
            return jobList;
        }
        var result = [];
        _.each(jobList, function(job) {
            var is_contains_in_customer_id = job.customer.customer_id.toLowerCase().indexOf(keyword) != -1;
            var is_contains_in_customer_name = job.customer.customer_name.toLowerCase().indexOf(keyword) != -1;
            var is_contains_in_own_machine_model = job.customer.own_machine_model.toLowerCase().indexOf(keyword) != -1;
            var is_contains_in_own_machine_number = job.customer.own_machine_number.toLowerCase().indexOf(keyword) != -1;
            if (is_contains_in_customer_id || is_contains_in_customer_name || is_contains_in_own_machine_model || is_contains_in_own_machine_number) {
                result.push(job);
            }
        });
        return result;
    }
});
app.controller('ReportController', function($scope, Pagination, $http, $q, $filter) {
    $scope.initializeJobReport = initializeJobReport;
    $scope.initializeRequisitionReport = initializeRequisitionReport;
    $scope.initializeCustomerReport = initializeCustomerReport;
    $scope.createJobReportCSV = createJobReportCSV;
    $scope.createRequisitionReportCSV = createRequisitionReportCSV;
    $scope.createCustomerHistoryReportCSV = createCustomerHistoryReportCSV;
	
	function clean(input){
		if(input){return input;}
		else return '';
	}

    function getDateLiteralForFileName() {
        var d = new Date();
        console.log(d);
        var result = d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate();
        console.log('getDateLiteralForFileName', result);
        return result;
    }

    function createJobReportCSV(data) {
        var header = ['Job No.', 'สถานะ', 'ลูกค้า', 'ช่าง', 'วันที่','ชื่ออะไหล่','รุ่น','จำนวน'];
        var exportData = [header];
        console.log(data);
        _.each(data, function(each) {
            exportData.push(['"' + clean(each.job_number) + '"', '"' + clean(each.status) + '"', '"' + clean(each.customerName) + '"', '"' + clean(each.technician_name) + '"', '"' + clean(each.requestDateForReport) + '"','','','']);
            if(each.itemList && each.itemList.length > 0){
                _.each(each.itemList, function(eachItem){
                    exportData.push(['"' + clean(each.job_number) + '"', '', '', '', '', '"' + clean(eachItem.part_name) + '"', '"' + clean(eachItem.used_in_model) + '"', '"' + clean(eachItem.quantity) + '"']);
                });
            }
        });
        streamDownloadFile(exportData, 'JobReport' + getDateLiteralForFileName() + '.csv');
    }

    function createRequisitionReportCSV(data) {
        var header = ['ใบเบิกหมายเลข', 'วันที่เบิก', 'ช่าง', 'ชื่ออะไหล่','รุ่น','จำนวน'];
        var exportData = [header];
        console.log(data);
        _.each(data, function(each) {
            exportData.push(['"' + clean(each.requisition_id) + '"', '"' + clean(each.requisition_date) + '"', '"' + clean(each.receiver_name) + '"', '', '', '']);
            if(each.requisition_item && each.requisition_item.length > 0){
                _.each(each.requisition_item, function(eachItem){
                    exportData.push(['"' + clean(each.requisition_id) + '"', '', '', '"' + clean(eachItem.part_name) + '"', '"' + clean(eachItem.used_in_model) + '"', '"' + clean(eachItem.borrow_amount) + '"']);
                });
            }
        });
        streamDownloadFile(exportData, 'RequisitionReport' + getDateLiteralForFileName() + '.csv');
    }

    function createCustomerHistoryReportCSV(data) {
        var header = ['รหัสลูกค้า', 'ชื่อลูกค้า', 'Job No.', 'ชื่ออะไหล่','มิเตอร์เริ่มต้น','มิเตอร์สิ้นสุด','อายุการใช้งาน','ส่วนต่างมิเตอร์','วันที่'];
        var exportData = [header];
        console.log(data);
        _.each(data, function(each) {
            exportData.push(['"' + clean(each.customer_id) + '"', '"' + clean(each.customer_name) + '"', '', '', '', '', '', '', '']);
            if(each.jobList && each.jobList.length > 0){
                _.each(each.jobList, function(eachItem){
                    exportData.push(['', '', '"' + clean(eachItem.job_number) + '"', '"' + clean(eachItem.part_name) + '"', '"' + clean(eachItem.start_meter) + '"', '"' + clean(eachItem.end_meter) + '"', '"' + clean(eachItem.deter_rate) + '"', '"' + clean(eachItem.meterDifference) + '"', '"' + clean(eachItem.request_date) + '"']);
                });
            }
        });
        streamDownloadFile(exportData, 'CustomerHistoryReport' + getDateLiteralForFileName() + '.csv');
    }


    function initializeJobReport() {
        var promises = [
            $http({
                method: 'GET',
                url: SERVER_ENDPOINT + GET_JOB_REQUEST_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
            })
        ];

        $q.all(promises).spread(function(jobRequestListResponse, customerListResponse, technicianListResponse) {
            $scope.technicianList = technicianListResponse.data;
            $scope.technicianMap = _.arrayToMap($scope.technicianList, 'user_id');
            console.log('$scope.technicianMap', $scope.technicianMap);

            $scope.customerList = customerListResponse.data;
            $scope.customerMap = _.arrayToMap($scope.customerList, 'customer_id');
            console.log('$scope.customerMap', $scope.customerMap);

            $scope.jobRequestList = jobRequestListResponse.data;
            _.each($scope.jobRequestList, function(job) {
                var customer = $scope.customerMap[job.customer_id];
                if (customer) {
                    job.customer = customer;
                }
                job.status = status = job.fixed == 1 ? 'เสร็จสมบูรณ์' : 'ต้องดำเนินการต่อ';
				try {
					job.customerName = job.customer_id + ' - ' + $scope.customerMap[job.customer_id].customer_name + ' - ' + $scope.customerMap[job.customer_id].own_machine_model + ' - ' + $scope.customerMap[job.customer_id].own_machine_number;
					job.technician_name = $scope.technicianMap[job.technician_id].nickname;
                job.requestDateForReport = $filter('customDate')(job.request_date);
				} catch(e){
					console.log(e);
				}
                
            });
			console.log('$scope.jobRequestList',$scope.jobRequestList);

            $scope.dataList = $scope.jobRequestList;
            $scope.pagination = new Pagination.getNew(10);
            $scope.dataLength = $scope.dataList.length;
            $scope.pagination.numPages = Math.ceil($scope.dataLength / $scope.pagination.perPage);
        });
    }

    function initializeRequisitionReport() {
        console.log('requisition');
        var promises = [
            $http({
                method: 'GET',
                url: SERVER_ENDPOINT + GET_REQUISITION_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
            })
        ];

        $q.all(promises).spread(function(requisitionListResponse, customerListResponse, technicianListResponse) {

            $scope.technicianList = technicianListResponse.data;
            $scope.technicianMap = _.arrayToMap($scope.technicianList, 'user_id');
            console.log('$scope.technicianMap', $scope.technicianMap);

            $scope.customerList = customerListResponse.data;
            $scope.customerMap = _.arrayToMap($scope.customerList, 'customer_id');
            console.log('$scope.customerMap', $scope.customerMap);
            $scope.requisitionList = requisitionListResponse.data;
            console.log(requisitionListResponse.data);
            // for(var i=0;i<100;i++){
            //     $scope.requisitionReport.requisitionList.push({});
            // }

            var dataList = $scope.requisitionList;
            //$scope.requisitionReport.pagination = new Pagination.getNew(10);
            //var dataLength = dataList.length;
            //$scoperequisitionReport.pagination.numPages = Math.ceil(dataLength / $scope.requisitionReport.pagination.perPage);
        });
    }

    function initializeCustomerReport() {
        var promises = [
            $http({
                method: 'GET',
                url: SERVER_ENDPOINT + GET_JOB_SPARE_PART_REPORT_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
            })
        ];

        $q.all(promises).spread(function(jobSparePartReportResponse, customerListResponse, technicianListResponse) {
            $scope.jobSparePartReportList = jobSparePartReportResponse.data;

            $scope.technicianList = technicianListResponse.data;
            $scope.technicianMap = _.arrayToMap($scope.technicianList, 'user_id');
            console.log('$scope.technicianMap', $scope.technicianMap);

            $scope.customerList = customerListResponse.data;
            console.log($scope.customerList);
            _.each($scope.customerList, function(customer) {
                var jobListOfThisCustomer = _.where($scope.jobSparePartReportList, { customer_id: customer.customer_id });
                console.log('jobListOfThisCustomer', jobListOfThisCustomer);
                if (jobListOfThisCustomer) {
                    customer.jobList = jobListOfThisCustomer;
                    for (var i = 0; i < jobListOfThisCustomer.length - 1; i++) {
                        var current = jobListOfThisCustomer[i];
                        var next = jobListOfThisCustomer[i + 1];

                        var isTheSameSparePart = current.part_number == next.part_number;
                        var sparePartHasDeterRate = current.deter_rate && next.deter_rate;
                        var meterDifference = current.start_meter - next.end_meter | 0;
                        var isBelowDeterRate = meterDifference < current.deter_rate;
                        if (isTheSameSparePart && sparePartHasDeterRate && isBelowDeterRate) {
                            current.isBelowDeterRate = true;
                            current.meterDifference = meterDifference;
                        }
                    }
                }
            });
        });
    }

    initializeJobReport();
});
