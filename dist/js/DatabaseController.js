app.controller('DatabaseController', function($scope, Pagination, $http) {
    $scope.month = month;
    $scope.monthShort = monthShort;
    $scope.weekdaysFull = weekdaysFull;
    $scope.weekdaysLetter = weekdaysLetter;

    $scope.newObj = {};

    $scope.currentObject = 'item';

    var getMethodMap = {
        'customer': CUSTOMER_LIST_ENDPOINT,
        'sparePart': SPARE_PART_LIST_ENDPOINT,
        'technician': TECHNICIAN_LIST_ENDPOINT
    };

    var createMethodMap = {
        'customer': CREATE_CUSTOMER_ENDPOINT,
        'sparePart': CREATE_SPARE_PART_ENDPOINT,
		'technician': CREATE_TECHNICIAN_ENDPOINT
    };

    var updateMethodMap = {
        'customer': UPDATE_CUSTOMER_ENDPOINT,
        'sparePart': UPDATE_SPARE_PART_ENDPOINT,
		'technician': UPDATE_TECHNICIAN_ENDPOINT
    };

    $scope.tableMetadata = {
        'technician': [{
            fieldName: 'id',
            label: 'เลขบัตรประชาชน',
            fieldType: 'text'
        },
		{
            fieldName: 'user_id',
            label: 'User Id (ห้ามซ้ำ)',
            fieldType: 'text',
			required: true
        },
		{
            fieldName: 'first_name',
            label: 'ชื่อ',
            fieldType: 'text'
        }, {
            fieldName: 'last_name',
            label: 'นามสกุล',
            fieldType: 'text'
        }, {
            fieldName: 'nickname',
            label: 'ชื่อเล่น',
            fieldType: 'text',
			required: true
        }, {
            fieldName: 'tel1',
            label: 'เบอร์โทรศัพท์ 1',
            fieldType: 'text'
        }, {
            fieldName: 'tel2',
            label: 'เบอร์โทรศัพท์ 2',
            fieldType: 'text'
        }],
        'sparePart': [{
            fieldName: 'part_name',
            label: 'ชื่ออะไหล่',
            fieldType: 'text',
			required: true
        }, {
            fieldName: 'used_in_model',
            label: 'รุ่น',
            fieldType: 'text',
			required: true
        }, {
            fieldName: 'part_type',
            label: 'ชนิด',
            fieldType: 'text'
        }, {
            fieldName: 'part_description',
            label: 'รายละเอียด',
            fieldType: 'text'
        }, {
            fieldName: 'deter_rate',
            label: 'อายุการใช้งาน (รอบมิเตอร์)',
            fieldType: 'number'
        }, {
            fieldName: 'quantity',
            label: 'จำนวน',
            fieldType: 'number',
			required: true
        }, {
            fieldName: 'price',
            label: 'ราคา',
            fieldType: 'number',
			required: true
        }],
        'customer': [{
            fieldName: 'customer_id',
            label: 'ID',
            fieldType: 'text',
			required: true
        }, {
            fieldName: 'customer_name',
            label: 'ชื่อ',
            fieldType: 'text',
			required: true
        }, {
            fieldName: 'customer_address',
            label: 'ที่อยู่',
            fieldType: 'text'
        }, {
            fieldName: 'department',
            label: 'แผนก',
            fieldType: 'text'
        }, {
            fieldName: 'own_machine_model',
            label: 'รุ่น',
            fieldType: 'text'
        }, {
            fieldName: 'own_machine_number',
            label: 'หมายเลขเครื่อง',
            fieldType: 'text'
        }, {
            fieldName: 'contact_name',
            label: 'ผู้ติดต่อ',
            fieldType: 'text'
        }, {
            fieldName: 'contact_tel',
            label: 'โทรศัพท์',
            fieldType: 'text'
        }, {
            fieldName: 'contact_tel_mobile',
            label: 'มือถือ',
            fieldType: 'text'
        }, {
            fieldName: 'remark',
            label: 'หมายเหตุ',
            fieldType: 'text'
        }]
    };

    $scope.getData = getData;
    $scope.create = create;
    $scope.edit = edit;
	$scope.deleteItem = deleteItem;
    $scope.toggleEditMode = toggleEditMode;

    $scope.getData();

    function getData() {
        $http({
            method: 'GET',
            url: SERVER_ENDPOINT + getMethodMap[$scope.currentObject]
        }).then(function successCallback(response) {
            console.log(response);
            $scope.dataList = response.data;
            $scope.pagination = new Pagination.getNew(10);
            $scope.dataLength = $scope.dataList.length;
            $scope.pagination.numPages = Math.ceil($scope.dataLength / $scope.pagination.perPage);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    function create() {
        console.log(JSON.stringify($scope.newObj));
		$scope.newObj.mark_delete = false;
        $http({
            method: 'POST',
            data: JSON.stringify($scope.newObj),
            url: SERVER_ENDPOINT + createMethodMap[$scope.currentObject]
        }).then(function successCallback(response) {
            console.log(response);
			$scope.newObj = {};
			$scope.getData();
        }, function errorCallback(response) {
            console.log(response);
        });
		
    }

    function edit(data){
        console.log(JSON.stringify(data));
		data.mark_delete = false;
        $http({
            method: 'POST',
            data: JSON.stringify(data),
            url: SERVER_ENDPOINT + updateMethodMap[$scope.currentObject]
        }).then(function successCallback(response) {
            console.log(response);
            $scope.getData();
        }, function errorCallback(response) {
            alert(response);
        });
    }
	
	function deleteItem(data){
		var confirmDelete = confirm("ยืนยันการลบ?");
		if(!confirmDelete){
			return;
		}
		data.mark_delete = true;
        console.log(JSON.stringify(data));
        $http({
            method: 'POST',
            data: JSON.stringify(data),
            url: SERVER_ENDPOINT + updateMethodMap[$scope.currentObject]
        }).then(function successCallback(response) {
            console.log(response);
            $scope.getData();
        }, function errorCallback(response) {
            alert(response);
        });
    }

    function toggleEditMode(data){
        if(data.editMode){
            data.editMode = false;
        } else {
            data.editMode = true;
        }
    }
});
