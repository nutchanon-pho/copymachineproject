app.filter('carryingSparePartFilter', function() {
    return function(sparePartList, $scope) {
        var result = [];
        if (!$scope.requestObj || !$scope.requestObj.selectedCustomer) {
            return;
        }
        var used_in_model = $scope.requestObj.selectedCustomer.own_machine_model.toUpperCase();
        var isUpdate = $scope.isUpdate;
        var job_number = $scope.requestObj.job_number;
        _.each(sparePartList, function(item) {
            var match_model = item.used_in_model.toUpperCase().indexOf(used_in_model) != -1;
            var match_job_number = !isUpdate || (item.job_number == job_number || !item.job_number);
            var is_all_used = item.remain_part == item.confirmedUsedQuantity;
            if (match_model && match_job_number && !is_all_used) {
                item.quantity = item.remain_part;
                result.push(item);
            }
        });
        return result;
    }
});
app.controller('JobController', function($scope, Pagination, $http, $q) {
    $scope.isUpdate = false;

    $scope.month = month;
    $scope.monthShort = monthShort;
    $scope.weekdaysFull = weekdaysFull;
    $scope.weekdaysLetter = weekdaysLetter;

    var promises = [
        $http({
            method: 'GET',
            url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + SPARE_PART_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
        }), $http({
            method: 'GET',
            url: SERVER_ENDPOINT + GET_CHECK_LIST_ENDPOINT
        })
    ];

    function initializeMasterData() {
        $q.all(promises).spread(function(technicianListResponse, sparePartListResponse, customerListResponse, checkListResponse) {
            $scope.technicianList = technicianListResponse.data;
            console.log('$scope.technicianList', $scope.technicianList);

            $scope.sparePartList = sparePartListResponse.data;
            $scope.sparePartMap = _.arrayToMap($scope.sparePartList, 'part_number');
            console.log('$scope.sparePartMap', $scope.sparePartMap);

            $scope.customerList = customerListResponse.data;
            console.log('$scope.customerList', $scope.customerList);

            $scope.pagination = new Pagination.getNew(10);
            $scope.pagination.numPages = Math.ceil($scope.customerList.length / $scope.pagination.perPage);

            $scope.requestObj = {
                selectedCustomer: null,
                itemList: [],
                request_date: new Date(),
                maintenanceOptionList: checkListResponse.data
            };

            _.each($scope.technicianList, function(user) {
                $q.all([$http({
                    method: 'GET',
                    params: { receiver_userid: user.user_id },
                    url: SERVER_ENDPOINT + GET_SPARE_PART_WITH_TECHNICIAN_ENDPOINT
                })]).spread(function(response) {
                    var sparePartOfThisTechnicianList = response.data;
                    _.each(sparePartOfThisTechnicianList, function(each){
						each.quantity = parseInt(each.remain_part);
					});
                    user.carryingSparePartList = sparePartOfThisTechnicianList;
                });
            });
            console.log('$scope.technicianList', $scope.technicianList);
            $scope.technicianMap = _.arrayToMap($scope.technicianList, 'user_id');
        });
    }
    initializeMasterData();

    var resetsparePart = function(sparePart) {
        sparePart.isUsed = false;
        sparePart.usedQuantity = sparePart.quantity;
        sparePart.confirmedUsedQuantity = 0;
    };

    angular.forEach($scope.carryingSparePartList, function(sparePart) {
        resetsparePart(sparePart);
    });

    $scope.useSparePart = useSparePart;
    $scope.unuseSparePart = unuseSparePart;
    $scope.selectCustomer = selectCustomer;
    $scope.submit = submit;
    $scope.saveRequest = saveRequest;
    $scope.getJobRequest = getJobRequest;
    $scope.selectTechnician = selectTechnician;
    $scope.clearCurrentJobRequest = clearCurrentJobRequest;
    $scope.sendEmailNotification = sendEmailNotification;

    function useSparePart(sparePart, quantity) {
        console.log('quantity', quantity);
		sparePart.quantity = parseInt(sparePart.quantity);
		console.log(sparePart.quantity);
        if (!sparePart.confirmedUsedQuantity) {
            sparePart.confirmedUsedQuantity = 0;
        }
		// console.log('sparePart before if', sparePart);
        var currentMaxValue = sparePart.quantity - sparePart.confirmedUsedQuantity;
		// console.log(quantity,currentMaxValue);
        if (quantity <= currentMaxValue) {
            sparePart.confirmedUsedQuantity += quantity;
            sparePart.usedQuantity = 0;
            // console.log('sparePart', sparePart);
            sparePart.isUsed = true;
        }
    }

    function unuseSparePart(sparePart) {
        resetsparePart(sparePart);
    }

    function selectCustomer(customer) {
        if ($scope.requestObj.selectedCustomer == customer) {
            $scope.requestObj.selectedCustomer = null;
            $scope.requestObj.customer_id = null;
        } else {
            $scope.requestObj.selectedCustomer = customer;
            $scope.requestObj.customer_id = customer.customer_id;
        }

        angular.forEach($scope.carryingSparePartList, function(sparePart) {
            resetsparePart(sparePart);
        });
    }


    function submit() {
        angular.forEach($scope.jobForm.$error.required, function(field) {
            field.$setDirty();
        });

        var isValidForm = $scope.jobForm.$valid;
        var isCustomerSelected = $scope.requestObj.selectedCustomer;
        var isTechnicianSelected = $scope.requestObj.technician_id;
        if (isValidForm && isCustomerSelected && isTechnicianSelected) {
            console.log('valid');
            $http({
                method: 'GET',
                params: { id: $scope.requestObj.customer_id },
                url: SERVER_ENDPOINT + GET_JOB_SPARE_PART_REPORT_BY_CUSTOMER_ENDPOINT
            }).then(function successCallback(response) {
                var sparePartHistory = response.data;
                $scope.suspiciousList = [];

                $scope.requestObj.itemList = [];
                angular.forEach($scope.requestObj.technician.carryingSparePartList, function(sparePart) {
                    if (sparePart.isUsed) {
                        console.log('sparePart', sparePart);
                        var usedSparePart = {
                            part_number: sparePart.part_number,
                            name: sparePart.part_name,
                            machineModel: sparePart.used_in_model,
                            quantity: sparePart.confirmedUsedQuantity,
                            requisition_id: sparePart.requisition_id,
                            item_line_id: sparePart.item_line_id
                        };
                        $scope.requestObj.itemList.push(usedSparePart);

                        if ($scope.requestObj.meter && $scope.requestObj.meter.start_meter && $scope.requestObj.meter.end_meter) {
                            var findResult = _.find(sparePartHistory, function(item) {
                                var isTheSameSparePart = item.part_number == usedSparePart.part_number;
                                var sparePartHasDeterRate = sparePart.deter_rate != null;
                                var isBelowDeterRate = $scope.requestObj.meter.start_meter - item.end_meter < sparePart.deter_rate;
                                var notComparingToItSelf = $scope.requestObj.job_number != item.job_number;
                                return isTheSameSparePart && sparePartHasDeterRate && isBelowDeterRate && notComparingToItSelf;
                            });
                            console.log('findResult', findResult);
							if(findResult != null){
									$scope.suspiciousList.push({
										history: findResult,
										current: usedSparePart
									});
							}
                        }
                    }
                });
                console.log('itemList', $scope.requestObj.itemList);
                console.log('$scope.requestObj', $scope.requestObj);
                console.log('suspiciousList', $scope.suspiciousList);
                if ($scope.suspiciousList && $scope.suspiciousList.length > 0) {
                    $scope.modalOpen = true;
                } else {
                    saveRequest();
                }
            }, function errorCallback(response) {
                alert('Error');
                console.log(response);
            });
        } else {
            if (!isValidForm) {
                alert('ใส่ค่ายังไม่ครบ');
            }
            if (!isCustomerSelected) {
                alert('ยังไม่เลือกลูกค้า');
            }
            if(!isTechnicianSelected){
                alert('ยังไม่เลือกช่าง');
            }
        }
    }

    function saveRequest() {
        var method;
        if (!$scope.isUpdate) {
            method = CREATE_JOB_REQUEST_ENDPOINT;
        } else {
            method = UPDATE_JOB_REQUEST_ENDPOINT;
        }       
		
        $http({
            method: 'POST',
            data: $scope.requestObj,
            url: SERVER_ENDPOINT + method
        }).then(function successCallback(response) {
            console.log(response);
            if(response.data && response.data.indexOf('Duplicate') != -1){
                alert('เลขใบงานซ้ำ ให้กดรูปแว่นขยายหลังจากกรอกเลขใบงานก่อนเสมอ เพื่อเช็คว่ามีใบงานนี้ในระบบแล้วหรือยัง');
            } else {
                alert('สำเร็จ');
                // location.reload();
            }
        }, function errorCallback(response) {
            alert('Error');
            console.log(response);
        });

        if($scope.suspiciousList && $scope.suspiciousList.length > 0){
            sendEmailNotification();
        }
    }

    function getJobRequest(job_number) {
        initializeMasterData();
        $http({
            method: 'GET',
            params: { id: job_number },
            url: SERVER_ENDPOINT + GET_JOB_REQUEST_ENDPOINT
        }).then(function successCallback(response) {
            console.log(response);
            var requestObj = response.data;
            if (requestObj) {
                $scope.requestObj.job_number = job_number;
                requestObj.request_date = Date.createFromMysql(requestObj.request_date);
                requestObj.fixed = requestObj.fixed && requestObj.fixed == 1 ? true : false;
                $scope.requestObj = requestObj;

                selectTechnician(requestObj.technician_id);
                var sparePartList = $scope.requestObj.technician.carryingSparePartList;
				console.log('sparePartList',sparePartList);
                _.each(requestObj.itemList, function(item) {
                    var targetIndex = _.findIndex(sparePartList, function(sparePart) {
                        return sparePart.part_number == item.part_number && sparePart.requisition_id == item.requisition_id && sparePart.item_line_id == item.item_line_id;
                    });
                    var foundTheRightSparePart = targetIndex != -1;
					console.log('foundTheRightSparePart',foundTheRightSparePart,item);
                    if (foundTheRightSparePart) {
						sparePartList[targetIndex].quantity += item.quantity;
						sparePartList[targetIndex].remain_part = sparePartList[targetIndex].quantity
                        useSparePart(sparePartList[targetIndex], item.quantity);
						console.log(sparePartList[targetIndex]);
                    }
                });

                var actualSparePartList = [];
                _.each(sparePartList, function(item){
                    item.quantity = parseInt(item.quantity);
                    var isUsed = item.isUsed;
                    var matchJobNumber = item.job_number == requestObj.job_number;
                    var hasNoJobNumber = !item.job_number;
                    var hasQuantity = item.quantity && item.quantity > 0;
                    if(isUsed || matchJobNumber || hasNoJobNumber && hasQuantity){
                        actualSparePartList.push(item);
                    }
                });
                console.log('actualSparePartList', actualSparePartList);
                $scope.requestObj.technician.carryingSparePartList = actualSparePartList;

                $scope.isUpdate = true;
            } else {
				alert("ไม่พบเลขใบงานนี้ในระบบ");
			}
            console.log('$scope.requestObj', $scope.requestObj);
        }, function errorCallback(response) {
            alert('Error');
            console.log(response);
        });
    }

    function selectTechnician(user_id) {
        var selectedTechnician = $scope.technicianMap[user_id];
        if (selectedTechnician) {
            $scope.requestObj.technician = selectedTechnician;
        }
        console.log('$scope.requestObj', $scope.requestObj);
    }

    function clearCurrentJobRequest() {
        $scope.isUpdate = false;
        _.each($scope.requestObj.technician.carryingSparePartList, function(sparePart) {
            unuseSparePart(sparePart);
        });
        $scope.requestObj = {};
        initializeMasterData();
    }

    function sendEmailNotification() {
        _.each($scope.suspiciousList, function(obj){
            $http({
                method: 'POST',
                data: {
                    current_job_number: $scope.requestObj.job_number,
                    past_job_number: obj.history.job_number,
                    customer_info: obj.history.customer_id
                },
                url: SERVER_ENDPOINT + SEND_EMAIL_ENDPOINT
            }).then(function successCallback(response) {
                console.log(response);
            }, function errorCallback(response) {
                console.log(response);
            });
        });
    }
});
