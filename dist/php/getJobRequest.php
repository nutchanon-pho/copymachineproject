<?php

require 'config.php';
// Create connection

if (!empty($_GET['id'])) {
    $jobNumber = $_GET['id'];
} else {
}

$stmt = $conn->prepare('SELECT * FROM `job_document` WHERE job_number = ?');
$stmt->bind_param('s', $jobNumber);

$stmt->execute();

$result = $stmt->get_result();

if ($result->num_rows == 0) {
    $stmt->close();
    $conn->close();

    return;
}
$jobRequest = $result->fetch_assoc();

//Technician
$insideStmt = $conn->prepare('	SELECT * FROM `technician` WHERE user_id = ?
								');
$insideStmt->bind_param('s', $userID);
$userID = $jobRequest['technician_id'];
$insideStmt->execute();

$resultRows = $insideStmt->get_result();
$checkList = array();
$row = $resultRows->fetch_assoc();
$jobRequest['technician'] = $row['nickname'];

//Customer
$insideStmt = $conn->prepare('SELECT * FROM `customer` WHERE customer_id = ?');
$insideStmt->bind_param('s', $customerID);
$customerID = $jobRequest['customer_id'];
$insideStmt->execute();
$customer = $insideStmt->get_result()->fetch_assoc();
$jobRequest['selectedCustomer'] = $customer;

//ItemList
$insideStmt = $conn->prepare("	SELECT j.part_number, part_name as 'name', used_in_model as 'machineModel', quantity, requisition_id, item_line_id
								FROM `job_spare_part` j
								LEFT JOIN spare_part s on j.part_number = s.part_number
								WHERE j.job_number = ?");
$insideStmt->bind_param('s', $jobNumber);
$insideStmt->execute();

$resultRows = $insideStmt->get_result();
$items = array();
while ($row = $resultRows->fetch_assoc()) {
    array_push($items, $row);
}

$jobRequest['itemList'] = $items;

//CheckList

$insideStmt = $conn->prepare('	SELECT c.checklist_number as checklist_number, c.checklist_detail, j.*
								FROM checklist c
								LEFT JOIN 
									(select job_checklist.checklist_number as cn, job_checklist.job_number from job_checklist where job_number =?) j 
								ON j.cn = c.checklist_number
								');
$insideStmt->bind_param('s', $jobNumber);
$insideStmt->execute();

$resultRows = $insideStmt->get_result();
$checkList = array();
while ($row = $resultRows->fetch_assoc()) {
    $check['checklist_number'] = $row['checklist_number'];
    $check['name'] = $row['checklist_detail'];
    if ($row['job_number'] == null) {
        $check['checked'] = false;
    } else {
        $check['checked'] = true;
    }
    array_push($checkList, $check);
}

$jobRequest['maintenanceOptionList'] = $checkList;

//Meter

$insideStmt = $conn->prepare('	SELECT * FROM `job_meter` WHERE job_number = ?
								');
$insideStmt->bind_param('s', $jobNumber);
$insideStmt->execute();

$resultRows = $insideStmt->get_result();
$meterList = null;
while ($row = $resultRows->fetch_assoc()) {
    unset($row['job_number']);
    $meterList = $row;
}

$jobRequest['meter'] = $meterList;

echo json_encode($jobRequest, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

$insideStmt->close();
$stmt->close();
$conn->close();
