<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `requisition` WHERE requisition_id = ?');
$stmt->bind_param('s', $requisitionId);

if (!empty($_GET['id'])) {
    $requisitionId = $_GET['id'];
}

$stmt->execute();

$result = $stmt->get_result();
$requisition = null;
while ($data = $result->fetch_assoc()) {
    $requisition = $data;
}

$stmt = $conn->prepare('SELECT * FROM requisition_item i LEFT JOIN spare_part s on s.part_number = i.part_number WHERE requisition_id = ?');
$stmt->bind_param('s', $requisitionId);

$stmt->execute();
$result = $stmt->get_result();
$requisitionItems = array();
while ($data = $result->fetch_assoc()) {
    unset($data['requisition_id']);
    $requisitionItems[] = $data;
}

$requisition['requisition_item'] = $requisitionItems;

echo json_encode($requisition, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

$stmt->close();
$conn->close();
