<?php

include 'config.php';

// Check connection
if ($conn->connect_error) {
    die('Connection failed: '.$conn->connect_error);
}
$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

$stmt = $conn->prepare('UPDATE requisition_item set return_amount = return_amount + ? WHERE requisition_id = ? and item_line_id = ?');
$stmt->bind_param('isi', $return_amount, $requisition_id, $item_line_id);

$requisition_id = $json_data['requisition_id'];
$item_line_id = $json_data['item_line_id'];
$return_amount = $json_data['return_amount'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt = $conn->prepare('UPDATE inventory set quantity = quantity + ? WHERE part_number = ?');
$stmt->bind_param("ii",$return_amount,$part_number);

$part_number = $json_data['part_number'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}


$stmt->close();
$conn->close();
