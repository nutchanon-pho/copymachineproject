<?php

include 'config.php';
// Create connection

$stmt = $conn->prepare('UPDATE `customer` SET `customer_id`=?,`customer_name`=?,`customer_address`=?,`department`=?,`contact_name`=?,`contact_tel`=?,`contact_tel_mobile`=?,`own_machine_model`=?,`own_machine_number`=?,`remark`=?, mark_delete =? WHERE `customer_id` = ?');

$stmt->bind_param('ssssssssssis', $customerID, $customerName, $customerAddress, $department, $contactName, $contactTel, $contact_mobile, $model, $machineNumber, $remark, $mark_delete, $oldCustomerID);

$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

echo $json_data;

$customerID = $json_data['customer_id'];
$customerName = $json_data['customer_name'];
$customerAddress = $json_data['customer_address'];
$department = $json_data['department'];
$contactName = $json_data['contact_name'];
$contactTel = $json_data['contact_tel'];
$contact_mobile = $json_data['contact_tel_mobile'];
$model = $json_data['own_machine_model'];
$machineNumber = $json_data['own_machine_number'];
$remark = $json_data['remark'];
$mark_delete = $json_data['mark_delete'];
$oldCustomerID = $customerID;
//$oldCustomerID = $json_data['old_id'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt->close();
$conn->close();
