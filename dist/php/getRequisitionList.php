<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `requisition`');

$stmt->execute();

$result = $stmt->get_result();
$requisitionMap = array();
while ($data = $result->fetch_assoc()) {
    // array_push($customerList, $data);
	$requisitionMap[$data['requisition_id']] = $data;
}

$innerStmt = $conn->prepare('SELECT * FROM `requisition_item` ri INNER JOIN spare_part sp ON ri.part_number = sp.part_number');
$innerStmt->execute();
$result = $innerStmt->get_result();
while ($item = $result->fetch_assoc()) {
	if(!is_null($requisitionMap[$item['requisition_id']])){
		if(!isset($requisitionMap[$item['requisition_id']]['requisition_item'])){
			$requisitionMap[$item['requisition_id']]['requisition_item'] = array();
		}
		array_push($requisitionMap[$item['requisition_id']]['requisition_item'], $item);
	}
}

$requisitionList = array_values($requisitionMap);

echo json_encode($requisitionList, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();
