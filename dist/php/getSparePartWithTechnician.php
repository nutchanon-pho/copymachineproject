<?php
require "config.php";
// Create connection


$stmt = $conn->prepare("select ri.requisition_id,ri.item_line_id ,ri.part_number,ri.job_number,r.receiver_userid,r.requisition_date,ri.borrow_amount, IFNULL(SUM(js.quantity), 0 ) as 'used', ri.return_amount, 
(ri.borrow_amount - IFNULL(SUM(js.quantity), 0 ) - ri.return_amount) as remain_part, sp.part_name, sp.used_in_model, sp.deter_rate
from requisition_item ri
left join requisition r
on r.requisition_id = ri.requisition_id
left join job_spare_part js
on (ri.job_number = js.job_number and ri.part_number = js.part_number) OR (ri.requisition_id = js.requisition_id and ri.item_line_id = js.item_line_id)
inner join spare_part sp
on ri.part_number = sp.part_number
where receiver_userid = ? AND sp.mark_delete = false
       GROUP BY ri.requisition_id, ri.item_line_id");
$stmt->bind_param("s",$receiver_userid);

if (!empty($_GET['receiver_userid'])) {
    $receiver_userid = $_GET['receiver_userid'];
}


$stmt->execute();

$result = $stmt->get_result();
$report = array();
while($data = $result->fetch_assoc()){
	array_push($report, $data);
}

echo json_encode($report,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();

?>