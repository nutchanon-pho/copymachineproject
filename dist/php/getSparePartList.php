<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT spare_part.part_number as id, part_name, used_in_model, deter_rate, inventory.quantity, inventory.price FROM spare_part INNER JOIN inventory ON spare_part.part_number = inventory.part_number AND spare_part.mark_delete = false');

$stmt->execute();

$result = $stmt->get_result();
$customerList = array();
while ($data = $result->fetch_assoc()) {
    array_push($customerList, $data);
}

echo json_encode($customerList, JSON_UNESCAPED_UNICODE);
$stmt->close();
$conn->close();
