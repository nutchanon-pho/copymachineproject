<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare("SELECT i.part_number as 'id', s.part_name as 'name', i.quantity, i.price as 'amount'
FROM `inventory` i
LEFT JOIN spare_part s
ON s.part_number = i.part_number");

$stmt->execute();

$result = $stmt->get_result();
$customerList = array();
while ($data = $result->fetch_assoc()) {
    array_push($customerList, $data);
}

echo json_encode($customerList, JSON_UNESCAPED_UNICODE);
$stmt->close();
$conn->close();
