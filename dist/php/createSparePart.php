<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT part_number FROM `spare_part`
						order by part_number DESC
						limit 0,1');
$stmt->execute();

$result = $stmt->get_result();
$lineNumber = 0;
while ($data = $result->fetch_assoc()) {
    $lineNumber = $data['part_number'];
}

$stmt = $conn->prepare('INSERT INTO `spare_part`(`part_number`, `part_name`, `part_description`, `part_type`, `used_in_model`, `deter_rate`) VALUES (?,?,?,?,?,?)');
$stmt->bind_param('issssi', $partNumber, $partName, $des, $partType, $model, $rate);

$inventoryQuery = $conn->prepare('INSERT INTO `inventory`(`part_number`, `quantity`, `price`) VALUES (?,?,?)');
$inventoryQuery->bind_param('iii', $partNumber, $quantity, $price);

$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

$partNumber = $lineNumber + 1;
$partName = $json_data['part_name'];
$des = $json_data['part_description'];
$partType = $json_data['part_type'];
$model = $json_data['used_in_model'];
$rate = $json_data['deter_rate'];
$quantity = $json_data['quantity'];
$price = $json_data['price'];

$stmt->execute();
$inventoryQuery->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}
if (!empty($inventoryQuery->error)) {
    echo 'Error : '.$inventoryQuery->error;
    die;
}

$stmt->close();
$inventoryQuery->close();
$conn->close();
