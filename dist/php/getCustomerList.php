<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `customer` WHERE mark_delete = false');

$stmt->execute();

$result = $stmt->get_result();
$customerList = array();
while ($data = $result->fetch_assoc()) {
    array_push($customerList, $data);
}

echo json_encode($customerList, JSON_UNESCAPED_UNICODE);
$stmt->close();
$conn->close();
