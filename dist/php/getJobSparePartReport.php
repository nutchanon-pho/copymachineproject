<?php
require "config.php";
// Create connection


$stmt = $conn->prepare("SELECT js.*,jd.customer_id,jd.request_date, jm.start_meter, jm.end_meter , sp.part_name , sp.deter_rate
        from job_spare_part js
        left join job_document jd on js.job_number = jd.job_number
        left join job_meter jm on jm.job_number = jd.job_number
        left join spare_part sp on js.part_number = sp.part_number
        order by jd.customer_id , js.part_number , jd.request_date DESC");


$stmt->execute();

$result = $stmt->get_result();
$report = array();
while($data = $result->fetch_assoc()){
	array_push($report, $data);
}

echo json_encode($report,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();

?>