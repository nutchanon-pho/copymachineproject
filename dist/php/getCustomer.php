<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `customer` WHERE customer_id = ? AND mark_delete = false');
$stmt->bind_param('s', $customer_id);

if (!empty($_GET['id'])) {
    $customer_id = $_GET['id'];
}

$stmt->execute();

$result = $stmt->get_result();
$customer = null;
while ($data = $result->fetch_assoc()) {
    $customer = $data;
}
echo json_encode($customer, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();
