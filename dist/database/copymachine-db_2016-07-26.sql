-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 26, 2016 at 03:47 PM
-- Server version: 5.5.35
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `copymachine`
--
CREATE DATABASE IF NOT EXISTS `copymachine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `copymachine`;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

DROP TABLE IF EXISTS `checklist`;
CREATE TABLE `checklist` (
  `checklist_number` int(11) NOT NULL,
  `checklist_detail` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `checklist`
--

TRUNCATE TABLE `checklist`;
-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` varchar(200) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `contact_name` varchar(30) NOT NULL,
  `contact_tel` varchar(20) NOT NULL,
  `customer_tel_mobile` varchar(20) DEFAULT NULL,
  `own_machine_model` varchar(10) NOT NULL,
  `own_machine_number` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `customer`
--

TRUNCATE TABLE `customer`;
--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `customer_address`, `department`, `contact_name`, `contact_tel`, `customer_tel_mobile`, `own_machine_model`, `own_machine_number`) VALUES
('A001-01', 'เอไอเอ (ชั่วคราว)', '181 อาคารเอ.ไอ.เอ.พาวเวอร์ ถ.สุรวงศ์\r\n', NULL, 'คุณเอ๋', '0-2638-7651', NULL, 'IR3570', 'KFW12067'),
('A002-01', 'อนามัยภัณฑ์', 'สำโรง ชั้น 2 สุขุมวิท 76', NULL, 'คุณพรพนา', '0-2393-1608 ', NULL, 'IR3045', 'MUZ00257');

-- --------------------------------------------------------

--
-- Table structure for table `job_checklist`
--

DROP TABLE IF EXISTS `job_checklist`;
CREATE TABLE `job_checklist` (
  `job_number` int(11) NOT NULL,
  `checklist_number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job_checklist`
--

TRUNCATE TABLE `job_checklist`;
-- --------------------------------------------------------

--
-- Table structure for table `job_spare_part`
--

DROP TABLE IF EXISTS `job_spare_part`;
CREATE TABLE `job_spare_part` (
  `job_number` int(11) NOT NULL,
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `job_spare_part`
--

TRUNCATE TABLE `job_spare_part`;
-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

DROP TABLE IF EXISTS `requisition`;
CREATE TABLE `requisition` (
  `requisition_id` int(8) NOT NULL,
  `requisition_date` date NOT NULL,
  `approver_userid` varchar(6) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `receiver_userid` int(6) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `giver_userid` int(6) DEFAULT NULL,
  `give_date` date DEFAULT NULL,
  PRIMARY KEY (`requisition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Requisition header';

--
-- Truncate table before insert `requisition`
--

TRUNCATE TABLE `requisition`;
-- --------------------------------------------------------

--
-- Table structure for table `requisition_item`
--

DROP TABLE IF EXISTS `requisition_item`;
CREATE TABLE `requisition_item` (
  `requisition_id` int(8) NOT NULL,
  `item_line_id` smallint(6) NOT NULL,
  `item` varchar(50) NOT NULL,
  `model` varchar(10) NOT NULL,
  `job_number` varchar(10) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `borrow_amount` smallint(6) NOT NULL,
  `return_amount` smallint(6) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `Remark` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `requisition_item`
--

TRUNCATE TABLE `requisition_item`;
-- --------------------------------------------------------

--
-- Table structure for table `spare_part`
--

DROP TABLE IF EXISTS `spare_part`;
CREATE TABLE `spare_part` (
  `part_number` int(10) NOT NULL COMMENT 'Spare part id',
  `part_name` int(11) NOT NULL,
  `part_type` int(11) NOT NULL,
  `used_in_model` varchar(10) NOT NULL,
  `deter_rate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `spare_part`
--

TRUNCATE TABLE `spare_part`;
-- --------------------------------------------------------

--
-- Table structure for table `technician`
--

DROP TABLE IF EXISTS `technician`;
CREATE TABLE `technician` (
  `user_id` int(6) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nickname` varchar(30) NOT NULL,
  `id` int(13) NOT NULL COMMENT 'Identification Number on Card',
  `tel1` varchar(13) DEFAULT NULL,
  `tel2` varchar(13) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `technician`
--

TRUNCATE TABLE `technician`;
-- --------------------------------------------------------

--
-- Table structure for table `่job_document`
--

DROP TABLE IF EXISTS `่job_document`;
CREATE TABLE `่job_document` (
  `job_number` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `issue_start_time` datetime NOT NULL,
  `issue_end_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `่job_document`
--

TRUNCATE TABLE `่job_document`;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
