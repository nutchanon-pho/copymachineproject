-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Oct 11, 2016 at 03:25 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `copymachine`
--
CREATE DATABASE IF NOT EXISTS `copymachine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `copymachine`;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

CREATE TABLE IF NOT EXISTS `checklist` (
  `checklist_number` int(11) NOT NULL,
  `checklist_detail` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `checklist`
--

INSERT INTO `checklist` (`checklist_number`, `checklist_detail`) VALUES
(1, 'เช็ดทำความสะอาดดรัม'),
(2, 'ทำความสะอาดชุด Feed กระดาษ'),
(3, 'ทำความสะอาดชุดโรเลอร์ชาร์จ'),
(4, 'ทำความสะอาดชุดเบลล์'),
(5, 'ทำความสะอาดชุด ADF'),
(6, 'ทำความสะอาดชุดกระจกสะท้อนภาพ'),
(7, 'ทำความสะอาดชุดทางเดินกระดาษ'),
(8, 'เปลี่ยนโรลเลอร์ฮีทซิ้ง'),
(9, 'เติมหมึก'),
(10, 'เทหมึกเสียทิ้ง');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` varchar(200) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `contact_name` varchar(30) NOT NULL,
  `contact_tel` varchar(20) NOT NULL,
  `contact_tel_mobile` varchar(20) DEFAULT NULL,
  `own_machine_model` varchar(10) NOT NULL,
  `own_machine_number` varchar(20) DEFAULT NULL,
  `remark` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `customer_address`, `department`, `contact_name`, `contact_tel`, `contact_tel_mobile`, `own_machine_model`, `own_machine_number`, `remark`) VALUES
('A001-01', 'เอไอเอ (ชั่วคราว)', '181 อาคารเอ.ไอ.เอ.พาวเวอร์ ถ.สุรวงศ์', NULL, 'คุณเอ๋', '0-2638-7651', NULL, 'IR3570', 'KFW12067', ''),
('A002-01', 'อนามัยภัณฑ์', 'สำโรง ชั้น 2 สุขุมวิท 76', NULL, 'คุณพรพนา', '0-2393-1608 ', NULL, 'IR3045', 'MUZ00257', '');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL COMMENT 'Number of parts left in inventory',
  `price` int(11) NOT NULL COMMENT 'Price of spar part'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`part_number`, `quantity`, `price`) VALUES
(1, 0, 2500),
(2, 446, 350);

-- --------------------------------------------------------

--
-- Table structure for table `job_checklist`
--

CREATE TABLE IF NOT EXISTS `job_checklist` (
  `job_number` varchar(20) NOT NULL,
  `checklist_number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_checklist`
--

INSERT INTO `job_checklist` (`job_number`, `checklist_number`) VALUES
('3423432', 1),
('3423432', 2),
('a1d5-as5w', 1),
('a1d5-as5w', 2),
('a1d5-as5w-1', 1),
('a1d5-as5w-1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `job_document`
--

CREATE TABLE IF NOT EXISTS `job_document` (
  `job_number` varchar(20) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `issue_start_time` varchar(10) DEFAULT NULL,
  `issue_end_time` varchar(10) DEFAULT NULL,
  `technician_id` varchar(10) NOT NULL,
  `request_date` datetime NOT NULL,
  `symptom` varchar(500) DEFAULT NULL,
  `root_cause` varchar(500) DEFAULT NULL,
  `solution` varchar(500) DEFAULT NULL,
  `otherMaintenance` varchar(500) DEFAULT NULL,
  `fixed` smallint(6) DEFAULT NULL,
  `nextAction` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_document`
--

INSERT INTO `job_document` (`job_number`, `customer_id`, `issue_start_time`, `issue_end_time`, `technician_id`, `request_date`, `symptom`, `root_cause`, `solution`, `otherMaintenance`, `fixed`, `nextAction`) VALUES
('a1d5-as5w', 'A001-01', NULL, NULL, 'jeng', '2016-08-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL),
('a1d5-as5w-1', 'A001-01', NULL, NULL, 'jeng', '2016-08-06 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL),
('test00001', 'A001-01', '9.10', '9.15', 'jeng', '2016-09-01 02:20:51', NULL, NULL, NULL, NULL, 1, NULL),
('111111', 'A002-01', '123123', '123123', 'jeng', '2016-09-01 02:24:06', NULL, NULL, NULL, NULL, NULL, NULL),
('45345d', 'A002-01', '345345', '345345', 'jeng', '2016-09-11 13:51:00', NULL, NULL, NULL, NULL, NULL, NULL),
('12092237', 'A002-01', '12312', '12312', 'jeng', '2016-09-12 15:37:51', NULL, NULL, NULL, NULL, 1, NULL),
('teepee', 'A002-01', '1234', '234234', 'jeng', '2016-09-14 15:41:10', NULL, NULL, NULL, NULL, 1, NULL),
('3423432', 'A001-01', '34234', '234234', 'jeng', '2016-10-08 02:03:40', NULL, NULL, NULL, NULL, 1, NULL),
('777777', 'A001-01', '10.30', '10.35', 'jeng', '2016-10-08 03:37:22', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_meter`
--

CREATE TABLE IF NOT EXISTS `job_meter` (
  `job_number` varchar(20) NOT NULL,
  `start_meter` int(11) DEFAULT NULL,
  `end_meter` int(11) DEFAULT NULL,
  `black_a4` int(11) DEFAULT NULL,
  `color_a4` int(11) DEFAULT NULL,
  `black_a3` int(11) DEFAULT NULL,
  `color_a3` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_meter`
--

INSERT INTO `job_meter` (`job_number`, `start_meter`, `end_meter`, `black_a4`, `color_a4`, `black_a3`, `color_a3`) VALUES
('a1d5-as5w', 0, 100, 100, 100, 100, 100),
('a1d5-as5w-1', 200, 450, 450, 450, 450, 450),
('test00001', 600, 898, 898, 898, 898, 898),
('45345d', NULL, NULL, NULL, NULL, NULL, NULL),
('12092237', 100, 103, NULL, NULL, NULL, NULL),
('teepee', 400, 405, NULL, NULL, NULL, NULL),
('3423432', NULL, NULL, NULL, NULL, NULL, NULL),
('777777', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_spare_part`
--

CREATE TABLE IF NOT EXISTS `job_spare_part` (
  `job_number` varchar(20) NOT NULL,
  `part_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `requisition_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `item_line_id` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_spare_part`
--

INSERT INTO `job_spare_part` (`job_number`, `part_number`, `quantity`, `requisition_id`, `item_line_id`) VALUES
('a1d5-as5w', 1, 3, '', 0),
('a1d5-as5w', 2, 5, '', 0),
('a1d5-as5w-1', 1, 3, '', 0),
('a1d5-as5w-1', 2, 5, '', 0),
('test00001', 1, 1, '', 1),
('111111', 1, 1, '234234-234', 1),
('12092237', 1, 1, '234234-234', 1),
('teepee', 1, 1, '9/12-23:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

CREATE TABLE IF NOT EXISTS `requisition` (
  `requisition_id` varchar(10) NOT NULL,
  `requisition_date` date NOT NULL,
  `approver_userid` varchar(6) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `receiver_userid` varchar(10) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `giver_userid` varchar(10) DEFAULT NULL,
  `give_date` date DEFAULT NULL,
  `receiver_onbehalf` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Requisition header';

--
-- Dumping data for table `requisition`
--

INSERT INTO `requisition` (`requisition_id`, `requisition_date`, `approver_userid`, `approval_date`, `receiver_userid`, `receive_date`, `giver_userid`, `give_date`, `receiver_onbehalf`) VALUES
('515-25761', '2016-08-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('test-test0', '2016-08-31', NULL, NULL, 'jeng', NULL, '123123', NULL, NULL),
('123-1232he', '2016-08-31', NULL, NULL, 'jeng', NULL, '12323', NULL, NULL),
('123-123', '2016-08-31', NULL, NULL, 'jeng', NULL, '123123', NULL, NULL),
('1234-1234', '2016-08-31', NULL, NULL, 'jeng', NULL, 'qweqwe', NULL, NULL),
('234234-234', '2016-09-01', NULL, NULL, 'jeng', NULL, '13123', NULL, NULL),
('123-1233', '2016-09-02', NULL, NULL, 'jeng', NULL, 'หกหกหกหก', NULL, NULL),
('123-12333', '2016-09-02', NULL, NULL, 'jeng', NULL, 'หกหกหกหก', NULL, NULL),
('234-sad', '2016-09-02', NULL, NULL, 'jeng', NULL, 'dasdasd', NULL, NULL),
('1234234-23', '2016-09-12', NULL, NULL, 'ake', NULL, 'ake', NULL, NULL),
('9/12-23:36', '2016-09-12', NULL, NULL, 'jeng', NULL, 'jeng', NULL, NULL),
('9/12-23:34', '0000-00-00', NULL, NULL, 'jeng', NULL, 'jeng', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `requisition_item`
--

CREATE TABLE IF NOT EXISTS `requisition_item` (
  `requisition_id` varchar(10) NOT NULL,
  `item_line_id` smallint(6) NOT NULL,
  `part_number` int(10) NOT NULL,
  `job_number` varchar(10) DEFAULT NULL,
  `customer_id` varchar(10) DEFAULT NULL,
  `borrow_amount` smallint(6) NOT NULL,
  `return_amount` smallint(6) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL,
  `Remark` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `requisition_item`
--

INSERT INTO `requisition_item` (`requisition_id`, `item_line_id`, `part_number`, `job_number`, `customer_id`, `borrow_amount`, `return_amount`, `value`, `Remark`) VALUES
('515-25761', 2, 2, '4', 'A001-02', 1, 0, 995, NULL),
('515-25761', 1, 1, '5', 'A001-01', 5, 0, 2500, NULL),
('test-test0', 1, 1, NULL, NULL, 0, 0, 5000, NULL),
('234234-234', 1, 1, NULL, NULL, 2, 0, 5000, NULL),
('123-12333', 1, 1, NULL, NULL, 3, 3, 7500, NULL),
('234-sad', 1, 1, '111111', NULL, 1, 2, 2500, NULL),
('1234234-23', 1, 2, NULL, NULL, 50, 0, 17500, NULL),
('9/12-23:36', 1, 1, NULL, NULL, 2, 0, 5000, NULL);

--
-- Triggers `requisition_item`
--
DELIMITER $$
CREATE TRIGGER `requisition_add` AFTER INSERT ON `requisition_item`
 FOR EACH ROW UPDATE inventory
	SET quantity = quantity - NEW.borrow_amount
	WHERE part_number = NEW.`part_number`
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `requisition_delete` AFTER DELETE ON `requisition_item`
 FOR EACH ROW UPDATE inventory
	SET quantity = quantity + OLD.borrow_amount
	WHERE part_number = OLD.`part_number`
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `spare_part`
--

CREATE TABLE IF NOT EXISTS `spare_part` (
  `part_number` int(10) NOT NULL COMMENT 'Spare part id',
  `part_name` varchar(100) NOT NULL,
  `part_description` varchar(100) DEFAULT NULL,
  `part_type` varchar(50) NOT NULL,
  `used_in_model` varchar(100) NOT NULL,
  `deter_rate` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spare_part`
--

INSERT INTO `spare_part` (`part_number`, `part_name`, `part_description`, `part_type`, `used_in_model`, `deter_rate`) VALUES
(1, 'BabyGanics Alcohol Free Foaming Hand Sanitizer', 'color ink', 'Ink', 'IR3045,IR3570', 500),
(2, 'Drum', NULL, 'Drum', 'IR3045,IR3570', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `technician`
--

CREATE TABLE IF NOT EXISTS `technician` (
  `user_id` varchar(6) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nickname` varchar(30) NOT NULL,
  `id` int(13) DEFAULT NULL COMMENT 'Identification Number on Card',
  `tel1` varchar(13) DEFAULT NULL,
  `tel2` varchar(13) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `technician`
--

INSERT INTO `technician` (`user_id`, `first_name`, `last_name`, `nickname`, `id`, `tel1`, `tel2`) VALUES
('jeng', NULL, NULL, 'เจ๋ง', NULL, '0934844940', NULL),
('ake', NULL, NULL, 'เอก', NULL, '0815495132', NULL),
('jsd', 'jack', 'sparow', 'เจเอสดี', NULL, '0934844940', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`part_number`);

--
-- Indexes for table `job_checklist`
--
ALTER TABLE `job_checklist`
  ADD PRIMARY KEY (`job_number`,`checklist_number`);

--
-- Indexes for table `job_document`
--
ALTER TABLE `job_document`
  ADD PRIMARY KEY (`job_number`);

--
-- Indexes for table `job_spare_part`
--
ALTER TABLE `job_spare_part`
  ADD PRIMARY KEY (`job_number`,`part_number`);

--
-- Indexes for table `requisition`
--
ALTER TABLE `requisition`
  ADD PRIMARY KEY (`requisition_id`);

--
-- Indexes for table `requisition_item`
--
ALTER TABLE `requisition_item`
  ADD UNIQUE KEY `Main` (`requisition_id`,`item_line_id`);

--
-- Indexes for table `spare_part`
--
ALTER TABLE `spare_part`
  ADD PRIMARY KEY (`part_number`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
