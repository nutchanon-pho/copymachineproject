function createCSVContent(exportData) {
    var BOM = '\uFEFF'; // This character is used to let any application openning this CSV file know that it is a UTF-8 encoding
    // var csvContent = "data:text/csv;utf-8," + BOM;
    // var csvContent = "data:text/csv;charset=utf-8," + BOM;
    var csvContent = '';
    exportData.forEach(function(infoArray, index) {
        dataString = infoArray.join(",");
        csvContent += index < exportData.length ? dataString + "\n" : dataString;
    });
    return csvContent;
}

function streamDownloadFile(dataAsArray, fileName) {
    var a = document.createElement('a');
    a.href = 'data:attachment/csv,\uFEFF' + encodeURIComponent(createCSVContent(dataAsArray));
    a.target = '_blank';
    a.download = fileName;
    // a.download = 'JobReport' + getDateLiteralForFileName() + '.csv';

    document.body.appendChild(a);
    a.click();
}

_.arrayToMap = function(sourceArray, fieldName) {
    var result = _.object(_.map(sourceArray, function(item) {
        return [item[fieldName], item]
    }));
    return result;
}

Date.createFromMysql = function(mysql_string) {
    var t, result = null;

    if (typeof mysql_string === 'string') {
        t = mysql_string.split(/[- :]/);

        //when t[3], t[4] and t[5] are missing they defaults to zero
        result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }

    return result;
}

var month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
var monthShort = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
var weekdaysFull = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัส', 'ศุกร์', 'เสาร์'];
var weekdaysLetter = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];

// var SERVER_ENDPOINT = 'http://192.168.1.21/copymachineproject/php/';
var SERVER_ENDPOINT = 'http://192.168.1.20/copymachineproject/php/';
//var SERVER_ENDPOINT = 'http://localhost:8888/copymachine/php/';

var CUSTOMER_LIST_ENDPOINT = 'getCustomerList.php';
var TECHNICIAN_LIST_ENDPOINT = 'getTechnicianList.php';
var SPARE_PART_LIST_ENDPOINT = 'getSparePartList.php';

var CREATE_CUSTOMER_ENDPOINT = 'createCustomer.php';
var CREATE_SPARE_PART_ENDPOINT = 'createSparePart.php';
var CREATE_TECHNICIAN_ENDPOINT = 'createTechnician.php';

var UPDATE_CUSTOMER_ENDPOINT = 'updateCustomer.php';
var UPDATE_SPARE_PART_ENDPOINT = 'updateSparePart.php';
var UPDATE_TECHNICIAN_ENDPOINT = 'updateTechnician.php';

var GET_SPARE_PART_WITH_TECHNICIAN_ENDPOINT = 'getSparePartWithTechnician.php';

var GET_JOB_REQUEST_ENDPOINT = 'getJobRequest.php';
var GET_JOB_REQUEST_LIST_ENDPOINT = 'getJobRequestList.php';
var CREATE_JOB_REQUEST_ENDPOINT = 'createJobRequest.php';
var UPDATE_JOB_REQUEST_ENDPOINT = 'updateJobRequest.php';

var CREATE_REQUISITION_ENDPOINT = 'createRequisition.php';
var UPDATE_REQUISITION_ENDPOINT = 'updateRequisition.php';

var GET_CHECK_LIST_ENDPOINT = 'getCheckList.php';

var GET_REQUISITION_LIST_ENDPOINT = 'getRequisitionList.php';
var GET_REQUESITION_BY_USER_ENDPOINT = 'getRequisitionByUser.php';

var RETURN_SPARE_PART_ENDPOINT = 'returnSparePart.php';

var GET_JOB_SPARE_PART_REPORT_ENDPOINT = 'getJobSparePartReport.php';
var GET_JOB_SPARE_PART_REPORT_BY_CUSTOMER_ENDPOINT = 'getJobSparePartReportByCustomer.php';

var SEND_EMAIL_ENDPOINT = 'sendMail.php';

var app = angular.module('InventoryApp', ['ngRoute', 'ui.materialize', 'simplePagination', 'mwl.bluebird']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/withdraw', {
            templateUrl: 'templates/withdraw.html',
            controller: 'WithdrawController'
        }).
        when('/database', {
            templateUrl: 'templates/database.html',
            controller: 'DatabaseController'
        }).
        when('/job', {
            templateUrl: 'templates/job.html',
            controller: 'JobController'
        }).
        when('/return', {
            templateUrl: 'templates/return.html',
            controller: 'ReturnController'
        }).
        when('/report', {
            templateUrl: 'templates/report.html',
            controller: 'ReportController'
        }).
        otherwise({
            redirectTo: '/withdraw'
        });
    }
]);

app.filter('customDate', function() {
    return function(input) {
        if (input != null) {
            var d = new Date(input);
            d.setHours(d.getHours() + 7 + 7);
            return d.toGMTString();
        } else {
            return null;
        }
    };
});
