app.filter('filterOutZeroQuantityItem', function() {
    return function(input) {
        var result = [];
        _.each(input, function(val) {
            if (val.quantity > 0) {
                result.push(val);
            }
        });
        return result;
    }
});
app.controller('ReturnController', function($scope, $http, $q) {
    $scope.submit = submit;

    function submit() {
        console.log('$scope.technician', $scope.technician);
        _.each($scope.technician.carryingSparePartList, function(item) {
            if (item.usedQuantity) {
                $q.all([$http({
                    method: 'POST',
                    data: {
                        requisition_id: item.requisition_id,
                        item_line_id: item.item_line_id,
                        return_amount: item.usedQuantity,
						part_number: item.part_number
                    },
                    url: SERVER_ENDPOINT + RETURN_SPARE_PART_ENDPOINT
                })]).spread(function(response) {
                    console.log(response);
					location.reload();
                });
            }
        });
    }

    function initializeMasterData() {
        var promises = [
            $http({
                method: 'GET',
                url: SERVER_ENDPOINT + TECHNICIAN_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + SPARE_PART_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + CUSTOMER_LIST_ENDPOINT
            }), $http({
                method: 'GET',
                url: SERVER_ENDPOINT + GET_JOB_REQUEST_LIST_ENDPOINT
            })
        ];

        $q.all(promises).spread(function(technicianListResponse, sparePartListResponse, customerListResponse, jobRequestListResponse) {
			$scope.jobRequestMap = _.indexBy(jobRequestListResponse.data, 'job_number');
			console.log(jobRequestListResponse);
			console.log($scope.jobRequestMap);
            $scope.technicianList = technicianListResponse.data;
            console.log('$scope.technicianList', $scope.technicianList);
            $scope.technicianMap = _.arrayToMap($scope.technicianList, 'user_id');
            var sparePartList = sparePartListResponse.data;
            console.log('sparePartList', sparePartList);
            $scope.customerList = customerListResponse.data;
            console.log('$scope.customerList', $scope.customerList);

            _.each($scope.technicianList, function(user) {
                $q.all([$http({
                    method: 'GET',
                    params: { receiver_userid: user.user_id },
                    url: SERVER_ENDPOINT + GET_SPARE_PART_WITH_TECHNICIAN_ENDPOINT
                })]).spread(function(response) {
                    console.log('carryingSparePartList', response);
                    user.carryingSparePartList = response.data;
                    _.each(user.carryingSparePartList, function(item) {
                        item.quantity = item.remain_part;
                    });
                });
            });
        });
    }
    initializeMasterData();
});
