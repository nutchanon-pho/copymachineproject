<?php

require 'config.php';
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die('Connection failed: '.$conn->connect_error);
}

$stmt = $conn->prepare('SELECT * FROM `customer` WHERE customer_id = ?');
$stmt->bind_param('s', $customer_id);

$customer_id = 'A001-01';

$stmt->execute();

$result = $stmt->get_result();

while ($data = $result->fetch_assoc()) {
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
}

$stmt->close();
$conn->close();

echo 'Connected successfully';
