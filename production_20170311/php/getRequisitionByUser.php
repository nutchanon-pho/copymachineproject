<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `requisition` WHERE receiver_userid = ?');
$stmt->bind_param('s', $receiver_userid);

if (!empty($_GET['receiver_userid'])) {
    $receiver_userid = $_GET['receiver_userid'];
}

$stmt->execute();

$result = $stmt->get_result();
$requisitionList = array();
while ($data = $result->fetch_assoc()) {
    array_push($requisitionList, $data);
}

for ($i = 0;$i < count($requisitionList);++$i) {

    $stmt = $conn->prepare('SELECT * FROM requisition_item i LEFT JOIN spare_part s on s.part_number = i.part_number WHERE requisition_id = ?');
    $stmt->bind_param('s', $requisitionId);

    $requisitionId = $requisitionList[$i]['requisition_id'];
    $stmt->execute();
    $result = $stmt->get_result();
    $requisitionItems = array();
    while ($data = $result->fetch_assoc()) {
        unset($data['requisition_id']);
        $data['quantity'] = $data['borrow_amount'];
        unset($data['borrow_amount']);
        $requisitionItems[] = $data;
    }

    $requisitionList[$i]['requisition_item'] = $requisitionItems;
}

echo json_encode($requisitionList, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

$stmt->close();
$conn->close();
