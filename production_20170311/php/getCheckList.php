<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT checklist_number, checklist_detail as name FROM `checklist`');

$stmt->execute();

$result = $stmt->get_result();
$checkList = array();
while ($data = $result->fetch_assoc()) {
    $data['checked'] = false;
    array_push($checkList, $data);
}

echo json_encode($checkList, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();
