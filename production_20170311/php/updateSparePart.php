<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('UPDATE `spare_part` SET `part_name`= ?,`part_description`= ?,`part_type`= ?,`used_in_model`= ?,`deter_rate`= ?, mark_delete = ? WHERE `part_number` = ?');
$stmt->bind_param('ssssiii', $partName, $des, $partType, $model, $rate, $mark_delete, $partNumber);

$inventoryQuery = $conn->prepare('INSERT INTO `inventory`(`part_number`, `quantity`, `price`) VALUES (?,?,?) ON DUPLICATE KEY UPDATE `quantity` = ?, `price` = ?');
$inventoryQuery->bind_param('iiiii', $partNumber, $quantity, $price,$quantity, $price);

$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

$partNumber = $json_data['id'];
$partName = $json_data['part_name'];
$des = $json_data['part_description'];
$partType = $json_data['part_type'];
$model = $json_data['used_in_model'];
$rate = $json_data['deter_rate'];
$quantity = $json_data['quantity'];
$price = $json_data['price'];
$mark_delete = $json_data['mark_delete'];

$stmt->execute();
$inventoryQuery->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}
if (!empty($inventoryQuery->error)) {
    echo 'Error : '.$inventoryQuery->error;
    die;
}

$stmt->close();
$inventoryQuery->close();
$conn->close();
