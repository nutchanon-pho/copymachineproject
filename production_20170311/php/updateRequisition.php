<?php

include 'config.php';

// Check connection
if ($conn->connect_error) {
    die('Connection failed: '.$conn->connect_error);
}
$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

$stmt = $conn->prepare('DELETE FROM requisition WHERE requisition_id = ?');
$stmt->bind_param('s', $requisition_id);
$requisition_id = $json_data['requisition_id'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt = $conn->prepare('INSERT INTO `requisition`(`requisition_id`, `requisition_date`, `approver_userid`, `approval_date`, `receiver_userid`, `receive_date`, `giver_userid`, `give_date`,`receiver_onbehalf`) VALUES (?,?,?,?,?,?,?,?,?)');

$stmt->bind_param('sssssssss', $requisition_id, $requisition_date, $approver_userid, $approval_date, $receiver_userid, $receive_date, $giver_userid, $give_date, $receiver_onbehalf);

$requisition_date = $json_data['requisition_date'];
$approver_userid = $json_data['approver_userid'];
$approval_date = $json_data['approval_date'];
$receiver_userid = $json_data['receiver_userid'];
$receive_date = $json_data['receive_date'];
$giver_userid = $json_data['giver_userid'];
$give_date = $json_data['give_date'];
$receiver_onbehalf = $json['receiver_onbehalf'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt = $conn->prepare('DELETE FROM requisition_item WHERE requisition_id = ?');
$stmt->bind_param('s', $requisition_id);

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}
$requisition_item = $json_data['requisition_item'];
$stmt = $conn->prepare('INSERT INTO `requisition_item`(`requisition_id`, `item_line_id`, `part_number`, `job_number`, `customer_id`, `borrow_amount`, `return_amount`, `value`, `Remark`) VALUES (?,?,?,?,?,?,?,?,?)');
$stmt->bind_param('siiisiiis', $requisition_id, $item_line_id, $part_number, $job_number, $customer_id, $borrow_amount, $return_amount, $value, $remark);

$item_line_id = 1;

foreach ($requisition_item as $item) {
    $part_number = $item['part_number'];
    $job_number = $item['job_number'];
    $customer_id = $item['customer_id'];
    $borrow_amount = $item['borrow_amount'];
    $return_amount = $item['return_amount'];
    $value = $item['value'];
    $remark = $item['remark'];

    $stmt->execute();
    if (!empty($stmt->error)) {
        echo 'Error : '.$stmt->error;
        die;
    }

    ++$item_line_id;
}

$stmt->close();
$conn->close();
