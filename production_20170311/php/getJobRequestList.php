<?php

require 'config.php';
// Create connection

$stmt = $conn->prepare('SELECT * FROM `job_document`');

$stmt->execute();

$result = $stmt->get_result();
$jobMap = array();
while($data = $result->fetch_assoc()){
	// array_push($jobList, $data);
	$jobMap[$data['job_number']] = $data;

}

$stmt2 = $conn->prepare('SELECT job_number, spare_part.part_number, part_name, used_in_model, deter_rate, quantity FROM `job_spare_part` INNER JOIN spare_part ON job_spare_part.part_number = spare_part.part_number');
$stmt2->execute();
$result = $stmt2->get_result();

while($item = $result->fetch_assoc()){
	// var_dump($item['job_number']);
	if(isset($jobMap[$item['job_number']])){
		if(!isset($jobMap[$item['job_number']]['itemList'])){
			$jobMap[$item['job_number']]['itemList'] = array();
		}
		array_push($jobMap[$item['job_number']]['itemList'], $item);
	}
}

$insideStmt = $conn->prepare('	SELECT c.checklist_detail, j.*
								FROM checklist c
								LEFT JOIN 
									(select * from job_checklist) j 
								ON j.checklist_number = c.checklist_number
								');
$insideStmt->execute();

$resultRows = $insideStmt->get_result();
// $checkList = array();
while($row = $resultRows->fetch_assoc()){
	$check['checklist_number'] = $row['checklist_number'];
	$check['name'] = $row['checklist_detail'];
	if($row['job_number'] == null){
		$check['checked'] = false;
	}else{
		$check['checked'] = true;
	}
	// array_push($checkList,$check);

	if(isset($jobMap[$row['job_number']])){
		if(!isset($jobMap[$row['job_number']]['maintenanceOptionList'])){
			$jobMap[$row['job_number']]['maintenanceOptionList'] = array();
		}
		array_push($jobMap[$row['job_number']]['maintenanceOptionList'], $row);
	}
}

$insideStmt = $conn->prepare("	SELECT * FROM `job_meter`");
$insideStmt->execute();
$resultRows = $insideStmt->get_result();
while($row = $resultRows->fetch_assoc()){
	if(isset($jobMap[$row['job_number']])){
		if(!isset($jobMap[$row['job_number']]['meter'])){
			$jobMap[$row['job_number']]['meter'] = array();
		}
		// array_push($jobMap[$row['job_number']]['meter'], $row);
		foreach ($row as $key => $value) {
			$jobMap[$row['job_number']]['meter'][$key] = $value;
			// print_r($jobMap[$row['job_number']]);
		}
	}
}


$jobList = array_values($jobMap);

echo json_encode($jobList,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$stmt->close();
$conn->close();
?>