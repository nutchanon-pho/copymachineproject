<?php

include 'config.php';
// Create connection

$stmt = $conn->prepare('INSERT INTO `technician`(`user_id`, `first_name`, `last_name`, `nickname`, `id`, `tel1`, `tel2`) VALUES (?,?,?,?,?,?,?)');

$stmt->bind_param('ssssiss', $userID, $firstName, $lastName, $nickName, $id, $tel1, $tel2);

$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

echo json_encode($json_data);
$userID = $json_data['user_id'];
$firstName = $json_data['first_name'];
$lastName = $json_data['last_name'];
$nickName = $json_data['nickname'];
$id = $json_data['id'];
$tel1 = $json_data['tel1'];
$tel2 = $json_data['tel2'];

echo $userID;
$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt->close();
$conn->close();
