<?php

include 'config.php';

// Check connection
if ($conn->connect_error) {
    die('Connection failed: '.$conn->connect_error);
}

$stmt = $conn->prepare('UPDATE `job_document` SET `job_number`=?,`customer_id`=?,`issue_start_time`=?,`issue_end_time`=?,`technician_id`=?,`request_date`=?,`symptom`=?,`root_cause`=?,`solution`=?,`otherMaintenance`=?,`fixed`=?,`nextAction`=? WHERE job_number = ?');

$stmt->bind_param('ssssssssssiss', $jobNumber, $customer_id, $issueStartTime, $issueEndTime, $techinicainID, $requestDate, $symtom, $rootCause, $solution, $otherMaintenance, $fixed, $nextAction, $jobNumber);

$data = file_get_contents('php://input');
$json_data = json_decode($data, true);

$jobNumber = $json_data['job_number'];
$customer_id = $json_data['customer_id'];
$issueStartTime = $json_data['issue_start_time'];
$issueEndTime = $json_data['issue_end_time'];
$techinicainID = $json_data['technician_id'];
$requestDate = $json_data['request_date'];
$symptom = $json_data['symptom'];
$rootCause = $json_data['root_cause'];
$solution = $json_data['solution'];
$otherMaintenance = $json_data['otherMaintenance'];
$fixed = $json_data['fixed'];
$nextAction = $json_data['nextAction'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt = $conn->prepare('DELETE FROM `job_spare_part` WHERE `job_number` = ?');
$stmt->bind_param('s', $jobNumber);
$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$itemList = $json_data['itemList'];
$stmt = $conn->prepare('INSERT INTO `job_spare_part`(`job_number`, `part_number`, `quantity`, `requisition_id`, `item_line_id`) VALUES (?,?,?,?,?)');
$stmt->bind_param('siisi', $jobNumber, $part_number, $quantity, $requisition_id, $item_line_id);

foreach ($itemList as $item) {
    $part_number = $item['part_number'];
    $quantity = $item['quantity'];
    $requisition_id = $item['requisition_id'];
    $item_line_id = $item['item_line_id'];
    $stmt->execute();
    if (!empty($stmt->error)) {
        echo 'Error : '.$stmt->error;
    } else {
        echo 'New records created successfully';
    }
}

$stmt = $conn->prepare('DELETE FROM `job_checklist` WHERE `job_number` = ?');
$stmt->bind_param('s', $jobNumber);
$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}
$maintenanceList = $json_data['maintenanceOptionList'];

$stmt = $conn->prepare('INSERT INTO `job_checklist`(`job_number`, `checklist_number`) VALUES (?,?)');
$stmt->bind_param('si', $jobNumber, $checklist_number);
$checklist_number = null;

foreach ($maintenanceList as $maintenanceItem) {
    $checklist_number = $maintenanceItem['checklist_number'];
    $checked = $maintenanceItem['checked'];
    if ($checklist_number != null && $checked) {
        $stmt->execute();
        if (!empty($stmt->error)) {
            echo 'Error : '.$stmt->error;
            die;
       	}
    }
}

$stmt = $conn->prepare('DELETE FROM `job_meter` WHERE `job_number` = ?');
$stmt->bind_param('s', $jobNumber);
$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$meter = $json_data['meter'];

$stmt = $conn->prepare('INSERT INTO `job_meter`(`job_number`, `start_meter`, `end_meter`, `black_a4`, `color_a4`, `black_a3`, `color_a3`) VALUES (?,?,?,?,?,?,?)');
$stmt->bind_param('siiiiii', $jobNumber, $startMeter, $endMeter, $blackA4, $colorA4, $blackA4, $colorA4);

$startMeter = $meter['start_meter'];
$endMeter = $meter['end_meter'];
$blackA4 = $meter['black_a4'];
$colorA4 = $meter['color_a4'];
$blackA4 = $meter['black_a3'];
$colorA4 = $meter['color_a3'];

$stmt->execute();
if (!empty($stmt->error)) {
    echo 'Error : '.$stmt->error;
    die;
}

$stmt->close();
$conn->close();
